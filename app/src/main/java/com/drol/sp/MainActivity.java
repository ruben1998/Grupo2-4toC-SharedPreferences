package com.drol.sp;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    EditText txtNombre, txtNumero;
    Button guardar, cargar;
    TextView resultado;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        txtNombre = (EditText) findViewById(R.id.txt_nombre);
        txtNumero = (EditText) findViewById(R.id.txt_numero);
        guardar = (Button) findViewById(R.id.btnGuardar);
        cargar = (Button) findViewById(R.id.btnCargar);
        resultado = (TextView) findViewById(R.id.tv_Mostrar);
        //estaba jugando ayer
        SharedPreferences sp = getSharedPreferences("Agenda", Context.MODE_PRIVATE);
        txtNombre.setText(sp.getString("nombre", ""));
        txtNumero.setText(sp.getString("numero",""));

        guardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences sp = getSharedPreferences("Agenda", Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sp.edit();
                String nombre = txtNombre.getText().toString();
                String numero = txtNumero.getText().toString();
                editor.putString("nombre", nombre);
                editor.putString("numero", numero);
                editor.commit();



                txtNombre.setText("");
                txtNumero.setText("");
            }
        });

        cargar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences sp = getSharedPreferences("Agenda", Context.MODE_PRIVATE);
                String nombre = sp.getString("nombre", "");
                String numero = sp.getString("numero", "");

                String preferencia = "Nombre: " + nombre + "\nNumero: " + numero;
                resultado.setText(preferencia);
            }
        });


    }
}
